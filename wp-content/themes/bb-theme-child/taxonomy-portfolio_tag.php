<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Silence is golden.
}
?>
<?php get_header(); ?>

<div class="fl-archive <?php FLLayout::container_class(); ?>">
	<div class="<?php FLLayout::row_class(); ?>">

		<?php //FLTheme::sidebar( 'left' ); ?>

		<div class="fl-content blog_detail_wrap"<?php FLTheme::print_schema( ' itemscope="itemscope" itemtype="https://schema.org/Blog"' ); ?>>

			<?php FLTheme::archive_page_header(); ?>

			<?php if ( have_posts() ) : ?>
               
				<?php
				 $i=1;
				while ( have_posts() ) :
					the_post();
					$featured_img = get_the_post_thumbnail_url();
					$ptags = wp_get_post_terms(get_the_ID(),'portfolio_tag');
					
					$tag_title="";
					if(count($ptags) > 0 ){
						foreach($ptags as $ptag){
							$tag_title .= "<a href='/portfolio_tag/".$ptag->slug."/'><small class='portfolio_category'>".$ptag->name."</small></a> / ";
						}
					}
					$tag_title = trim($tag_title , " / ");
					?>
					<div class="width_100">
					
					
					<?php if($i % 2 == "1"){
					
						if($featured_img){
							?>
							<div class="width_50 align_left">
								<a href="<?php echo $featured_img;?>" rel="lightbox">
									<div class="tumbnail_img">
										<img src="<?php echo $featured_img; ?>" width="500" height="500" alt="Thumbnail" class="thumbnailImg" />
										<div class="uabb-background-mask zoom-out lightbox_icon ">
											<div class="uabb-inner-mask">
												<div class="uabb-overlay-icon">
													<i class="ua-icon ua-icon-Maximize"></i>
												</div>
											</div>
										</div>
									</div>
								</a>
							</div>
							<?php
						}
                       ?>
					   
					   <div class="width_50 align_right postTitle">
					   	<a href="<?php the_permalink();?>"><h2 class="portfolio_title"><?php the_title();?></h2></a>
						 <span class="category_title"><?php echo $tag_title;?></span>  
						</div>

					   <?php
					}else{
						?>
					   <div class="width_50 align_right postTitle">
					   	<a href="<?php the_permalink();?>"><h2 class="portfolio_title"><?php the_title();?></h2></a>
						 <span class="category_title"> <?php echo $tag_title;?></span>  
						</div>
					   <?php
						if($featured_img){
							?>
							<div class="width_50 align_left">
								<a href="<?php echo $featured_img;?>" rel="lightbox">
									<div class="tumbnail_img">
										<img src="<?php echo $featured_img; ?>"  width="500" height="500" alt="Thumbnail" class="thumbnailImg" />
										<div class="uabb-background-mask zoom-out lightbox_icon ">
											<div class="uabb-inner-mask">
												<div class="uabb-overlay-icon">
													<i class="ua-icon ua-icon-Maximize"></i>
												</div>
											</div>
										</div>
									</div>
								</a>
							</div>
							<?php
						}
                       
					}
					$i++;

					?>
				</div>
					<?php //get_template_part( 'content', get_post_format() ); ?>
				<?php endwhile; ?>

				<?php FLTheme::archive_nav(); ?>

			<?php else : ?>

				<?php get_template_part( 'content', 'no-results' ); ?>

			<?php endif; ?>

		</div>

		<?php //FLTheme::sidebar( 'right' ); ?>

	</div>
</div>

<?php get_footer(); ?>
