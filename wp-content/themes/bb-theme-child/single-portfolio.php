<?php get_header(); ?>

<div class="container" style="max-width: none !important;">
	<div class="row">

		<?php //FLTheme::sidebar( 'left' ); ?>

		<div class="fl-content ">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					the_content();
					// get_template_part( 'content', 'single' );
				endwhile;
			endif;
			?>
		</div>

		<?php //FLTheme::sidebar( 'right' ); ?>

	</div>
</div>

<?php get_footer(); ?>
