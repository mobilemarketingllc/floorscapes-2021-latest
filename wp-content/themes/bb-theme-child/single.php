<?php get_header(); ?>
<?php $img = get_the_post_thumbnail_url();
$img = "";
?>
		
<div style="width:100%;height:500px;text-align:center;padding: 120px 20px; <?php if($img){?>background-image: url('<?php echo $img;?>'); <?php }else{ ?> background-color:#e6e6e6; <?php } ?>">
    <div style="width:100%;height:100%;display: flex;align-items: center;justify-content: center;" class=" blog-post-banner">
	    <div class="inteco-single-article-head-left">
		   <p class="inteco-single-article-date-day"><?php echo get_the_date('j');?></p>
		   <p class="inteco-single-article-date-month"><?php echo get_the_date('M');?></p>
		   </div>
		<div class="inteco-single-article-head-right">
	 		<h1 class="blog-heading"><?php the_title();?></h1>
			 <p>
			 	<span class="inteco-head"><i class="ua-icon ua-icon-Newspaper" aria-hidden="true"></i> <?php the_author();?></span>
			 	<span class="inteco-head"><i class="far fa-folder" aria-hidden="true"></i> <?php the_category('|');?></span>
				<span class="inteco-head"><i class="ua-icon ua-icon-tag" aria-hidden="true"></i> <?php the_tags('',',');?></span>
				<span class="inteco-head"><i class="far fa-comment" aria-hidden="true"></i> <?php echo get_comments_number();?></span>
			 </p>
		 </div>
	</div>
</div>

<div class="container">
	<div class="row">

		<?php //FLTheme::sidebar( 'left' ); ?>

		<div class="fl-content <?php FLTheme::content_class(); ?>">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					the_content();
				endwhile;
			endif;
			?>
		</div>

		<?php FLTheme::sidebar( 'right' ); ?>

	</div>
</div>

<?php get_footer(); ?>
